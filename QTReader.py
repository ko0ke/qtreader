from PyQt5 import QtWidgets
from PyQt5.QtWidgets import (QMessageBox)
from PyQt5 import QtCore
import serial.tools.list_ports
import os
import subprocess
from QTDesign import Ui_MainWindow  # importing our generated file
 
import sys
 
class Formulario(QtWidgets.QMainWindow):
 
    def __init__(self):
 
        super(Formulario, self).__init__()
 
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)

        # Ocultamos el frame del modo lectura de los cierres por defecto
        self.ui.frameModoLecturaCierre.hide()

        # Boton Salir
        self.ui.pushButtonSalir.clicked.connect(self.pushButton_Salir_Clicked)
        self.ui.actionSalir.triggered.connect(self.pushButton_Salir_Clicked)

        # RadioButton Tipos de Curva
        self.ui.radioButtonTM1.clicked.connect(lambda:self.radioButtonTipoCurva_Clicked(self.ui.radioButtonTM1))
        self.ui.radioButtonTM2.clicked.connect(lambda:self.radioButtonTipoCurva_Clicked(self.ui.radioButtonTM2))
        self.ui.radioButtonCierre.clicked.connect(lambda:self.radioButtonTipoCurva_Clicked(self.ui.radioButtonCierre))

        # Seteamos la fecha actual en la Fecha Fin
        self.ui.dateTimeEditFechaFin.setDateTime(QtCore.QDateTime.currentDateTime())
    
        # Añadimos en el comboBox los puerto COM disponibles en el sistema
        puertos = serial.tools.list_ports.comports()
        for port in sorted(puertos):
            self.ui.comboBoxPuerto.addItem(port)

        # Boton Ping
        self.ui.pushButtonPing.clicked.connect(self.ping)

        # Boton Refrescar puertos COM
        self.ui.pushButtonRefrescarPuertos.clicked.connect(self.addPuertosCOM)

        # Boton Descargar
        self.ui.pushButtonDescargar.clicked.connect(self.descargar)

    # Metodo que cerrara el formulario
    def pushButton_Salir_Clicked(self):
        self.close()

    # Metodo que ocultara/mostrara el frame de modo lectura correspondiente
    def radioButtonTipoCurva_Clicked(self,rb):
        if rb.text() == "Cierre (C1)": 
            self.ui.frameModoLecturaCierre.show()
            self.ui.frameModoLecturaCurva.hide()
        else:
            self.ui.frameModoLecturaCierre.hide()
            self.ui.frameModoLecturaCurva.show()

    # Ping
    def ping(self):
        if self.ui.lineEditIPoTelefono.text() != "":
            respuesta = os.system("ping " + self.ui.lineEditIPoTelefono.text())
            msg = QMessageBox()
            msg.setWindowTitle("Ping")
            if respuesta == 0:
                #msg.setIcon(QMessageBox.Information)
                msg.setText("Ping correcto!")
            else:
                #msg.setIcon(QMessageBox.Critical)
                msg.setText("Ping incorrecto!")
            msg.setStandardButtons(QMessageBox.Ok)
            msg.exec_()

    # Metodo que añadira en el comboBox los puertos COM disponibles en el sistema
    def addPuertosCOM(self):
        puertos = serial.tools.list_ports.comports()
        for port in sorted(puertos):
            self.ui.comboBoxPuerto.addItem(port)

    # Metodo que lanzara el programa de lectura
    def descargar(self):
        p = subprocess.Popen("D:\\Projects\\SMReader\\bin\\smreader.exe \"10.2.0.238;40000;250;21600;1;7;2;filename;d;2;1;1;04/03/2019 11:01;04/03/2019 11:30\"", stdout=subprocess.PIPE, shell=True)
        (output, err) = p.communicate()
        p_status = p.wait()
        print ("Command output : ", output)
        print ("Command exit status/return code : ", p_status)
        
 
app = QtWidgets.QApplication([])
 
application = Formulario()
application.show()
 
sys.exit(app.exec())